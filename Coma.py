import numpy as np
import scipy as sp
import quadpy


class Coma:
    def __init__(self, outgassing=6e27, cos_scale=3, neutral_velocity=1, radius=2000):
        """

        :param outgassing: outgassing rate (s^-1)
        :param cos_scale the asymmetricity of the coma factor - for symetric case choose 1 (unitless)
        :param neutral_velocity: the neutral velocity (m/s)
        """
        self.outgassing = outgassing
        self.cos_scale = cos_scale
        self.neutral_velocity = neutral_velocity
        self.normalisation_factor = self.calc_normalisation_factor()
        self.column_density_vectorised = np.vectorize(self.column_density_slice)
        self.radius = radius

    def normalisation_factor_function(self, y, x):
        # the normalisation factor integrated function
        n_factor_func_val = ((1 + self.cos_scale * np.cos(np.cos(y) * np.sin(x) / 2)) * (np.sin(x)) / (4 * np.pi))
        return n_factor_func_val

    def calc_normalisation_factor(self):
        s_inverse = sp.integrate.dblquad(self.normalisation_factor_function, 0, np.pi, 0, 2 * np.pi)
        n_factor = 1 / s_inverse[0]
        return n_factor

    def neutral_number_density(self, x_in, y_in, z_in):
        r = np.sqrt(x_in ** 2 + y_in ** 2 + z_in ** 2)
        solar_zenith = np.arccos(x_in / r)
        density = (self.outgassing * self.normalisation_factor / (4 * np.pi * (r ** 2) * self.neutral_velocity)) * (
                1 + self.cos_scale * np.cos(solar_zenith / 2))
        nuclues_accounted_density = (np.heaviside((r - self.radius), 0) * density + 1)
        return nuclues_accounted_density

    def column_density_slice(self, x, y, z_slice):
        output = sp.integrate.quad_vec(lambda x_in: self.neutral_number_density(x_in, y, z_slice), x, sp.inf)
        line_integral = output[0]
        return line_integral
