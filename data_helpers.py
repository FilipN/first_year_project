import pandas as pd
import numpy as np

def load_solar_dataframe(location):
    df = pd.read_csv(location, sep="\s+", skiprows=7, header=None,
                       names=['wavelength_min', 'wavelength_max',
                              'irradiance', 'flux'])
    df = df.assign(mid_wavelength = (df.wavelength_min+df.wavelength_max)/2)
    return df


def get_closest_val(df, column, values):
    """
    Returns the closest value in the dataframe

    Args:
    df - pandas.DataFrame
    column - string name of column of the df var in which to search the value
    values - np.array of integers to search for
    """
    if np.array(values) == ():
        values = [values]
    df2merge = pd.DataFrame(data=np.array(values), columns=[column])
    return pd.merge_asof(df2merge, df,
                         on=[column], direction='nearest')
