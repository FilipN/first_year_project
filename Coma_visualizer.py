import numpy as np
import scipy as sp
from data_helpers import get_closest_val
import numpy as np


class Coma_visualizer:
    def __init__(self, coma, sampling, max_distance, solar_df, h2o_df, slice_z):
        """

        :param coma: object made by Coma class
        :param sampling: integer - the resolution of the displayed image
        :param max_distance: integer the max distance (m) visualized in the image in each direction
        :param solar_df: pd.DataFrame containing the unattenuated solar flux values
        :param h2o_df: pd.DataFrame containing the absorption and ionization rate values
        :param slice_z: location at which z location to do the slice
        """
        self.coma = coma
        self.sampling = sampling
        self.max_distance = max_distance
        self.solar_df = solar_df
        self.h2o_df = h2o_df
        self.slice_z = slice_z
        y = x = np.linspace(-self.max_distance, self.max_distance, self.sampling)
        self.x_axis, self.y_axis = np.meshgrid(x, y)
        self.column_density = self.calc_column_density()
        self.neutral_number_density = self.calc_neutral_number_density()

    def calc_neutral_number_density(self):
        return self.coma.neutral_number_density(self.x_axis, self.y_axis, self.slice_z)

    def calc_column_density(self):
        c_density = self.coma.column_density_vectorised(self.x_axis, self.y_axis, self.slice_z)
        c_density[(np.abs(self.y_axis) < self.coma.radius) & (self.x_axis < 0)] = 0
        c_density[np.sqrt(self.y_axis**2+self.x_axis**2) <= self.coma.radius] = 0
        return c_density

    def optical_depth(self, wavelength):

        if len(np.array(wavelength)) > 1: # if the wavelength is an array
            photo_absorption = get_closest_val(self.h2o_df, 'wavelength', wavelength).photo_absorbtion.values * 1e-4
            return photo_absorption[:, None, None] * self.column_density
        else: # if it is single value
            photo_absorption = get_closest_val(self.h2o_df, 'wavelength', wavelength).photo_absorbtion.values[0] * 1e-4
            return photo_absorption * self.column_density

    def attenuated_solar_flux(self, wavelength):
        flux = get_closest_val(self.solar_df, 'mid_wavelength', wavelength).flux.values[0]
        attenuated_flux = np.exp(-self.optical_depth(wavelength)) * flux
        attenuated_flux[self.optical_depth(wavelength) > 2] = 0
        return attenuated_flux

    def electron_production_rate(self, sources_list):
        ep_rate = []
        for source in sources_list: # integrated for each source
            def integrated_func(wavelength):
                ion_rate = \
                    get_closest_val(self.h2o_df, 'wavelength', [wavelength])[''.join([source, '_ionization'])].values[0]
                return self.attenuated_solar_flux([wavelength]) * ion_rate * self.neutral_number_density

            source_ep_rate = sp.integrate.quad_vec(integrated_func, 0.1, 98.2, limit=10)
            ep_rate.append(source_ep_rate[0])
        return np.stack(ep_rate)
